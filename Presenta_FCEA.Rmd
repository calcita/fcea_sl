---
title: "Usos del software libre en el Estado y en las organizaciones sociales" 
subtitle:  "VIII Jornadas académicas (FCEA-UdelaR)"
author: 
  - Gabriela Mathieu \newline
  - \scriptsize{8 de noviembre de 2018}  \normalsize
header-includes:
- \usepackage[utf8]{inputenc}
# - \usepackage[spanish]{babel}
- \usepackage{latexsym}
- \usepackage{hyperref}
- \usepackage{amsmath}
- \usepackage{makeidx}
- \usepackage{amssymb}
- \usepackage{float} 
- \usepackage{graphicx}
- \usepackage{keystroke}
- \usepackage{graphics}
- \usepackage{colortbl}
- \usepackage{color}
- \usepackage{url}
- \usepackage{multicol}
- \hypersetup{colorlinks=true, linkcolor=blue, filecolor=magenta, urlcolor=cyan,  }
- \usepackage{caption}
- \captionsetup[figure]{labelformat=empty}  
- \renewenvironment{verbatim}{\footnotesize\oldverbatim}{\endoldverbatim}
- \usepackage{fancyhdr}
# - \titlegraphic{\flushleft \includegraphics[scale=0.6]{images/CC-BY-SA.pdf} \hfill \includegraphics[width=4cm]{images/fcea.png}}
- \titlegraphic{ \includegraphics{images/logos.png}}
output: 
        beamer_presentation: 
                theme: "Rochester" 
                colortheme: "dolphin"
                fonttheme: "structurebold"
---

### Software libre en el Estado \hfill ![](images/courthouse.png){width=0.6cm}

> - ![](images/profile.png){width=0.6cm}\hspace{0.2cm} El Estado administra **información** pública y privada de toda la sociedad \vspace{0.2cm}
> - ![](images/caution.png){width=0.6cm}\hspace{0.2cm} El uso de software propietario o privativo para el almacenamiento y manipulación de esa información genera **riesgos** y altos **costos** \vspace{0.2cm}
> - ![](images/balancing.png){width=0.6cm}\hspace{0.2cm} El uso de software libre además de ofrecer **seguridad** garantiza la **democratización** del acceso a la información y los sistemas del Estado \vspace{0.2cm}
> - ![](images/piggy-bank.png){width=0.6cm}\hspace{0.2cm} El uso de software libre, dado que en muchos casos es también gratuito, genera **ahorro**


### Software Libre y Formatos Abiertos en el Estado \hfill ![](images/libra.png){width=0.6cm}

**[Ley Nº 19.179](http://www.impo.com.uy/bases/leyes/19179-2013)** (diciembre 2013)

El software libre es el que está licenciado de forma que cumpla simultáneamente las siguientes condiciones:

> -  ![](images/idea.png){width=0.5cm} \hspace{0.2cm} Puede ser **usado** para cualquier propósito \vspace{0.2cm}
> -  ![](images/copy.png){width=0.5cm} \hspace{0.2cm} Puede ser **copiado** y **distribuido** \vspace{0.2cm}
> -  ![](images/binary-code.png){width=0.5cm} \hspace{0.2cm} Acceso a su código fuente para ser **estudiado** y **cambiado**  \vspace{0.2cm}
> -  ![](images/share-alike.png){width=0.5cm} \hspace{0.2cm} **Liberación** de dichas **mejoras** a la ciudadanía 

> -  ![](images/padlock.png){width=0.5cm} \hspace{0.2cm} El software privativo es aquel que priva de alguna de las cuatro libertades inherentes al software libre.
 
### Software Libre y Formatos Abiertos en el Estado \hfill ![](images/libra.png){width=0.6cm}

**[Ley Nº 19.179](http://www.impo.com.uy/bases/leyes/19179-2013)** (diciembre 2013)



> -  ![](images/open-sign.png){width=0.5cm} \hspace{0.2cm} Los formatos abiertos son formas de manejo y almacenamiento de los datos en los que se conoce su estructura y se permite su **modificación** y **acceso** no imponiéndose ninguna restricción para su uso. 

> -  ![](images/file.png){width=0.5cm} \hspace{0.2cm} Los datos almacenados en formatos abiertos no requieren de software privativo para ser utilizados.

> -  ![](images/mozilla.png){width=0.5cm} \hspace{0.2cm} El Estado deberá distribuir la información en al menos un formato, estándar y abierto, accesible desde al menos un navegador de código abierto

<!-- ” y “que cuando se contraten licencias de software se dará preferencia a licenciamientos de software libre“, en el caso de que se contrate software privativo se deberá fundamentar la razón -->

### AGESIC \hfill ![](images/agesic.png){width=2.5cm}

[AGESIC (Agencia de Gobierno Electrónico y Sociedad de la Información y del Conocimiento)](https://www.agesic.gub.uy/innovaportal/v/7359/11/agesic/catalogo-de-software-libre.html?currentpage=1) promueve el uso del software libre dentro del Estado y el desarrollo del software público.


El *Software Público Uruguayo* es aquel software de interés para el Estado y la sociedad uruguaya, que puede ser utilizado, compartido, modificado y distribuido libremente.

### Cátalogo de software libre (AGESIC)

![](images/sl1.png) ![](images/sl2.png)


### MIDES \hfill ![](images/mides_dinem.png){width=3cm}

Obtención y manejo de datos 

* Bases de datos sobre personas y hogares
* Georreferenciación de las viviendas
* Visita a los hogares
* Aplicación de cuestionario
* Alimentación de las bases de datos
* Procesamiento y Análisis

\centering ![](images/database_color.png){width=0.5cm} \hspace{0.2cm} ![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm} ![](images/map.png){width=0.5cm}\hspace{0.2cm}  ![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm} ![](images/house_blue.png){width=0.5cm}\hspace{0.2cm} ![](images/right-arrow.png){width=0.5cm}\hspace{0.2cm}![](images/typing.png){width=0.5cm} \hspace{0.2cm}![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm}![](images/database_cloud.png){width=0.5cm} \hspace{0.2cm}![](images/right-arrow.png){width=0.5cm}\hspace{0.2cm}![](images/analytics.png){width=0.5cm}\hspace{0.2cm}![](images/refresh.png){width=0.5cm}

###  Software libre para bases de datos 

\centering ![](images/database_color.png){width=0.5cm} \hspace{0.2cm} ![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm} ![](images/map_bn.png){width=0.5cm}\hspace{0.2cm}  ![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm} ![](images/house_blue_bn.png){width=0.5cm}\hspace{0.2cm} ![](images/right-arrow.png){width=0.5cm}\hspace{0.2cm}![](images/typing_bn.png){width=0.5cm} \hspace{0.2cm}![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm}![](images/database_cloud_bn.png){width=0.5cm} \hspace{0.2cm}![](images/right-arrow.png){width=0.5cm}\hspace{0.2cm}![](images/analytics_bn.png){width=0.5cm}\hspace{0.2cm}![](images/refresh.png){width=0.5cm}


* PostgreSQL + PostGIS
    * Se obtienen los datos para georreferenciar
    * Base de datos geográficas de diferentes fuentes
    * Realización de geoprocesos

![](images/postgresql.png){width=1.2cm} \hspace{0.5cm} ![](images/postgis.png){width=1.2cm}

<!-- ![](images/mysql.png){width=2cm} -->

### Software libre para georreferenciación y análisis espacial 

\centering ![](images/database_color_bn.png){width=0.5cm} \hspace{0.2cm} ![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm} ![](images/map.png){width=0.5cm}\hspace{0.2cm}  ![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm} ![](images/house_blue_bn.png){width=0.5cm}\hspace{0.2cm} ![](images/right-arrow.png){width=0.5cm}\hspace{0.2cm}![](images/typing_bn.png){width=0.5cm} \hspace{0.2cm}![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm}![](images/database_cloud_bn.png){width=0.5cm} \hspace{0.2cm}![](images/right-arrow.png){width=0.5cm}\hspace{0.2cm}![](images/analytics_bn.png){width=0.5cm}\hspace{0.2cm}![](images/refresh.png){width=0.5cm}

* gvSIG o QGIS
    * Gestión de recorridos 
    * Mapas y visualizadores
* R
    * Gestión de grandes BD
    * Conexión con PostGIS
    * Localización óptima
    * Mapas y visualizadores
    
* Leaflet 
    * Mapas y visualizadores
    
\centering ![](images/gvsig.png){width=1cm} \hspace{0.5cm} ![](images/qgis.png){width=2cm} \hspace{0.5cm} ![](images/R.png){width=1.3cm}\hspace{0.5cm} ![](images/leaflet.png){width=3cm}  

### Ejemplo: Integración de R, PostgreSQL y PostGIS \hfill ![](images/database.png){width=0.5cm} 

Los paquetes RODBC y RPostgreSQL de R permiten enviar consultas a las bases. Con ellas podemos traer las capas como objetos de R, procesarlas y guardar resultados en las bases de datos.

![](images/RPostgis.png){width=9cm}

### Software libre para análisis y visualización

\centering ![](images/database_color_bn.png){width=0.5cm} \hspace{0.2cm} ![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm} ![](images/map_bn.png){width=0.5cm}\hspace{0.2cm}  ![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm} ![](images/house_blue_bn.png){width=0.5cm}\hspace{0.2cm} ![](images/right-arrow.png){width=0.5cm}\hspace{0.2cm}![](images/typing_bn.png){width=0.5cm} \hspace{0.2cm}![](images/right-arrow.png){width=0.5cm} \hspace{0.2cm}![](images/database_cloud_bn.png){width=0.5cm} \hspace{0.2cm}![](images/right-arrow.png){width=0.5cm}\hspace{0.2cm}![](images/analytics.png){width=0.5cm}\hspace{0.2cm}![](images/refresh.png){width=0.5cm}

* R 
    * análisis
    * visualización (gráficos y mapas)
* R + RStudio (shiny, leaflet) 
    * visualizadores web
* R + LaTex 
    * reportes reproducibles

![](images/R.png){width=1.3cm} \hspace{0.7cm}![](images/rstudio.png){width=1cm} \hspace{0.5cm} ![](images/latex.png){width=2cm} \hspace{0.5cm} ![](images/leaflet.png){width=3cm}  

### Ejemplo: Reportes reproducibles \hfill ![](images/pdf.png){width=0.5cm} ![](images/html.png){width=0.5cm} 

![](images/informe1.png) ![](images/informe2.png)

<!-- ![](images/python.png){width=2cm} -->

<!-- ![](images/julia.png){width=2cm} -->

### Ejemplo: Análisis espacial y salida gráfica \hfill ![](images/pin.png){width=0.5cm}

![](images/mapa.jpeg)

### Software libre en organizaciones sociales

* El uso de software libre es también una cuestión ética
* Muchas organizaciones sociales utilizan a menudo software propietario y servicios gratuitos "regalando" información y vulnerando sus propios principios
* Existen alternativas libres y gratuitas a Whatsapp, Google Drive, Dropbox, etc.

### Encuentro de Feministas Diversas \hfill ![](images/EFD.png){width=1.2cm}

* El EFD es una colectiva feminista, anticapitalista, antirracista y antipatriarcal
* El EFD se organiza en comisiones y proyectos
* Es una colectiva autofinanciada
* En el espacio virtual de trabajo promueve el cuidado de los datos y el uso de software libre

### Software libre para la comunicación, recolección y almacenamiento de datos

> -  Telegram: sistema de **mensajería** \vspace{0.5cm}
> -  Nextcloud: creación y gestión de **nube** de almacenamiento propia \vspace{0.5cm}
> -  LimeSurvey: creación de **encuestas** \vspace{0.5cm}
> -  Omeka: plataforma web para **colecciones** digitales \vspace{0.5cm}
> -  Wordpress: creación y administración de **sitio web** \vspace{0.5cm}

\centering ![](images/telegram.png){width=1.2cm} \hspace{0.2cm} ![](images/nextcloud.png){width=2cm}  \hspace{0.2cm} ![](images/limesurvey.png){width=2cm} \hspace{0.2cm}  ![](images/omeka.png){width=1cm}\hspace{0.2cm} ![](images/wordpress.png){width=3cm} \hspace{0.2cm} 
<!-- (CMS:  sistema de gestión de contenidos) -->

### Nube

![](images/nube.png)

### Encuestas

![](images/encuestas.png)

### Biblioteca digital

![](images/biblioteca.png)

### Web (Proyecto Harta)

![](images/harta.png)


<!-- ### Software libre para ofimática  -->


<!-- ![](images/libreoffice.png){width=2cm} -->




<!-- ### Varios  -->

<!-- ![](images/moodle.png){width=2cm} -->

<!-- ![](images/octave.png){width=2cm} -->


